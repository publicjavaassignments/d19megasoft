package sample;

import javafx.collections.ObservableList;

import java.util.Observable;

public interface RentABook {

    void booksToRent(ObservableList allBooks);

    public void addBookToRentList();

}