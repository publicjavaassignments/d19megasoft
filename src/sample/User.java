package sample;

import javafx.collections.ObservableList;

public class User {

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ObservableList getRentedBooks() {
        return rentedBooks;
    }

    public void setRentedBooks(ObservableList rentedBooks) {
        this.rentedBooks = rentedBooks;
    }

    private String username;
    private String password;

    private ObservableList rentedBooks;
}
