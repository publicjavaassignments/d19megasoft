package sample;

import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;

public class LoginController {

    @FXML
    TextField usernameFXID;
    @FXML
    PasswordField passwordFXID;

    private ArrayList<User> userList;

    public void handleLogin() {
        userList.add(new User(usernameFXID.getText(), passwordFXID.getText()));
    }

}